import accessories as ac
import os

class generationLogger(object):

    def __init__(self, fileName, subdir, writer):
        self.subdir = subdir
        self.hatiar = ac.accessories()
        self.writer = writer
        self.inputFile = fileName
        self.data = self.hatiar.getPanda(fileName)
        self.hatiar.prettyPrint(self.data.columns)

    def plotBar(self, keys, index, plotName):
        if self.data.dropna().empty:
            print('Data frame is empty')
            return

        data = self.data[keys].set_index(index)
        self.writer.writeSheet(data, plotName)
        keys.remove(index[0])

        if len(data) <= 300:
            self.hatiar.printBar(data, index, keys, self.subdir+'/'+plotName)
        else:
            dataList = self.hatiar.dataToChunkList(data, chunkSize=300)
            if not os.path.exists(self.subdir+'/'+plotName):
                os.makedirs(self.subdir+'/'+plotName)

            i=1
            for dataChunk in dataList:
                self.hatiar.printBar(dataChunk, index, keys, self.subdir+'/'+plotName+'/'+plotName+'-'+i.__str__())
                i += 1


    def plotLag(self, key, plotName):
        if self.data.dropna().empty:
            print('Data frame is empty')
            return

        data = self.data[key]
        self.hatiar.printLag(data, self.subdir+'/'+plotName)


    def writeGroup(self):
        if self.data.dropna().empty:
            print('Data frame is empty')
            return

        self.writer.writeSheet(self.data, 'Generation Log Data')
        self.writer.writeSheet(self.hatiar.getDescription(self.data), 'Basic Statistics')
        self.writer.writeSheet(self.data.corr(), 'pearson-correlation')
        self.writer.writeSheet(self.data.corr(method='spearman'), 'spearman-correlation')
        print('group data written for '+self.subdir+'/'+self.inputFile)