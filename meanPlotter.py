import accessories as ac

class meanPlotter(object):

    def __init__(self, fileName, subdir, writer):
        self.fileName = fileName
        self.subdir = subdir
        self.writer = writer
        self.hatiar = ac.accessories()
        self.data = self.hatiar.getPanda(fileName)
        self.hatiar.prettyPrint(self.data.columns)

    def plotBar(self, keys, index, plotName):
        if self.data.dropna().empty:
            print('Data frame is empty')
            return

        data = self.data[keys].set_index(index)
        self.writer.writeSheet(data, plotName)
        keys.remove(index[0])
        self.hatiar.printBar(data, index, keys, self.subdir+'/'+plotName)

    def plotLag(self, key, plotName):
        if self.data.dropna().empty:
            print('Data frame is empty')
            return

        data = self.data[key]
        self.hatiar.printLag(data, self.subdir+'/'+plotName)

    def writeMeanData(self):
        if self.data.dropna().empty:
            print('Data frame is empty')
            return

        self.writer.writeSheet(self.data, 'ALS_Mean_Data')
        self.writer.writeSheet(self.hatiar.getDescription(self.data), 'Basic Statistics')
        self.writer.writeSheet(self.data.corr(), 'pearson-correlation')
        self.writer.writeSheet(self.data.corr(method='spearman'), 'spearman-correlation')
        print("saved mean_data to excel...")
