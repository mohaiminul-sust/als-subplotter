import os
from matplotlib import style
import generationLogger as gl
import excelWriter as ew
import infoLogger as il
import alsPlotter as ap
import meanPlotter as mp

path = 'ALS_data'

# using plotting style for pandas
style.use('ggplot')

for subdir, dirs, files in os.walk(path):
    if not os.path.exists(subdir + '/plots') or os.path.exists(subdir + '/meanplots'):
        if 'plots' not in subdir:
            print('.......................' + subdir + '........................')
            os.makedirs(subdir + '/plots')

    for file in files:
        if file == 'als_mean.csv':
            writer = ew.excelWriter(subdir + "/plots/mean_logger.xlsx")
            mpl = mp.meanPlotter(fileName=subdir+'/'+file, subdir=subdir+'/plots', writer=writer)

            mpl.writeMeanData()

            mpl.plotBar(['dna_len', 'copy_avg_am'], ['dna_len'], 'dna_len_vs_copy_am')
            mpl.plotBar(['dna_len', 'copy_avg_gm'], ['dna_len'], 'dna_len_vs_copy_gm')
            mpl.plotBar(['dna_len', 'copy_avg_hm'], ['dna_len'], 'dna_len_vs_copy_hm')

            mpl.plotLag(['copy_avg_am'], 'arithmatic_mean_lag')
            mpl.plotLag(['copy_avg_gm'], 'geometric_mean_lag')
            mpl.plotLag(['copy_avg_hm'], 'harmonic_mean_lag')

            writer.saveWriter()

        if file == 'ALS_data.csv':
            writer = ew.excelWriter(subdir + "/plots/als_logger.xlsx")
            alspl = ap.alsPlotter(fileName=subdir + '/' + file, subdir=subdir + '/plots', writer=writer)

            # write als data to excel
            alspl.writeAlsData()

            # plot bar graphs against run time
            # alspl.plotBar(['number_of_run', 'total_thread', 'total_male', 'total_female'], ['number_of_run'],
            #               'runtime vs total_male_female')
            # alspl.plotBar(['number_of_run', 'copy_dna_number', 'total_thread'], ['number_of_run'],
            #               'runtime vs total_copy_dna_num')
            # alspl.plotBar(['number_of_run', 'copy_dna_number'], ['number_of_run'], 'runtime vs copy_dna_num')
            # alspl.plotBar(['number_of_run', 'time'], ['number_of_run'], 'runtime vs time')
            # alspl.plotBar(['number_of_run', 'latest_generation'], ['number_of_run'], 'runtime vs latest_gen')
            # alspl.plotBar(['number_of_run', 'total_thread'], ['number_of_run'], 'runtime vs total_thread')


            # plot bar graphs against gene sequence length
            # alspl.plotBar(['dna_len', 'total_thread', 'total_male', 'total_female'], ['dna_len'],
            #               'DNA_len vs total_male_female')
            # alspl.plotBar(['dna_len', 'copy_dna_number', 'total_thread'], ['dna_len'],
            #               'DNA_len vs total_copy_dna_num')
            # alspl.plotBar(['dna_len', 'copy_dna_number'], ['dna_len'], 'DNA_len vs copy_dna_num')
            # alspl.plotBar(['dna_len', 'time'], ['dna_len'], 'DNA_len vs time')
            # alspl.plotBar(['dna_len', 'latest_generation'], ['dna_len'], 'DNA_len vs latest_gen')
            # alspl.plotBar(['dna_len', 'total_thread'], ['dna_len'], 'DNA_len vs total_thread')

            #lagplots
            alspl.plotLag(['copy_dna_number'], 'Copy DNA lagplot')
            alspl.plotLag(['total_male'], 'male lagplot')
            alspl.plotLag(['total_female'], 'female lagplot')
            alspl.plotLag(['latest_generation'], 'generation lagplot')

            writer.saveWriter()

        if file == 'Generation_log.csv':
            writer = ew.excelWriter(subdir + '/plots/gen_logger.xlsx')
            genLog = gl.generationLogger(fileName=subdir + '/' + file, subdir=subdir + '/plots', writer=writer)

            # write group data to excel file with calculations and statistical methods
            genLog.writeGroup()

            # plot bar graph against generation number for session logs
            # genLog.plotBar(['generation_number', 'offsprings_per_generation'], ['generation_number'],
            #                'gen vs offspring')
            # genLog.plotBar(['generation_number', 'male_offsprings_per_generation'], ['generation_number'],
            #                'gen vs male_offspring')
            # genLog.plotBar(['generation_number', 'female_offsprings_per_generation'], ['generation_number'],
            #                'gen vs female_offspring')
            # genLog.plotBar(['generation_number', 'female_offsprings_per_generation', 'male_offsprings_per_generation'],
            #                ['generation_number'], 'gen vs male_female_offspring')

            # lagplot
            genLog.plotLag(['male_offsprings_per_generation'], 'male offspring per gen')
            genLog.plotLag(['female_offsprings_per_generation'], 'female offspring per gen')
            genLog.plotLag(['offsprings_per_generation'], 'offspring per gen')

            writer.saveWriter()

        if file == 'ThreadInfo.csv':
            writer = ew.excelWriter(subdir + '/plots/thread_logger.xlsx')
            threadLog = il.infoLogger(fileName=subdir + '/' + file, subdir=subdir + '/plots', writer=writer)

            # write group data to excel file with calculations and statistical methods
            threadLog.writeInfo()

            # plot bar graph fro thread based analysis
            # threadLog.plotBar(['thread name', 'total move'], ['thread name'], 'move-per-thread')
            # threadLog.plotBar(['thread name', 'alive_time'], ['thread name'], 'time-per-thread')
            # threadLog.plotBar(['thread name', 'total_food_count', 'total move'], ['thread name'],
            #                   'food-move-per-thread')
            # threadLog.plotBar(['thread name', 'total_children_count'], ['thread name'], 'offspring-per-thread')

            #lagplot
            threadLog.plotLag(['total_children_count'], 'offspring per thread')

            writer.saveWriter()

